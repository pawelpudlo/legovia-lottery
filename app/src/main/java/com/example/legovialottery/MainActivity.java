package com.example.legovialottery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    List<String> playerList = new ArrayList<>();

    private String names = "";

    TextView players;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refresh();
    }

    public void addPlayers(View view) {

        EditText player = findViewById(R.id.editText);

        String name = "" + player.getText();

        this.playerList.add(name);

    }

    private void refreshRun(int milisecound) {

        final Handler handler = new Handler();

        final Runnable run = new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        };
        handler.postDelayed(run, milisecound);
    }

    public void refresh() {

        players = findViewById(R.id.players);

        names = "";
        for (int i = 0; i < playerList.size(); i++) {
            names += i + 1 + "." + playerList.get(i) + "\n";
        }

        players.setText(names);

        refreshRun(1000);
    }

    public void drawWinners(View view) {

        Intent intent = new Intent(this, EndActivity.class);

        if(playerList.size() < 4){
            players = findViewById(R.id.players);
            players.setText("Minimum 4 graczy");
        }else{

            Random generator = new Random();
            int index,x=0;

            int[] table = {-1,-1,-1};
            String key;
            String name;

            for(int i=1; i<=3; i++) {

                do{
                    index = generator.nextInt(playerList.size());

                }while(index == table[0] || index==table[1] || index==table[2]);

                table[x] = index;
                x++;

                key = "" + i;
                name = playerList.get(index);

                saveData(key,name);
            }

            startActivity(intent);
        }


    }

    public void saveData(String key,String output) {
        SharedPreferences sharedPreferences = getSharedPreferences("TEST", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, output);
        editor.apply();
    }

    public void back(View view) {
        if(playerList.size()!=0){
            playerList.remove(playerList.size()-1);
        }
    }
}