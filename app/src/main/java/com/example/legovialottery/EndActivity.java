package com.example.legovialottery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

public class EndActivity extends AppCompatActivity {

    TextView one;
    TextView two;
    TextView three;
    TextView endTime;
    private int time = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);
        one = findViewById(R.id.firstPlace);
        two = findViewById(R.id.secondPlace);
        three = findViewById(R.id.thirdPlace);
        endTime = findViewById(R.id.licznik);
        loadData();
        refresh();
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("TEST", MODE_PRIVATE);
        String first = sharedPreferences.getString("1", "");
        String second = sharedPreferences.getString("2", "");
        String third = sharedPreferences.getString("3", "");


        one.setText(first);
        two.setText(second);
        three.setText(third);
    }

    private void refreshRun(int milisecound) {

        final Handler handler = new Handler();

        final Runnable run = new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        };
        handler.postDelayed(run, milisecound);
    }

    public void refresh() {
        endTime.setText(time+"");
        if(time==-1){
            one.setVisibility(View.VISIBLE);
            two.setVisibility(View.VISIBLE);
            three.setVisibility(View.VISIBLE);
            endTime.setVisibility(View.INVISIBLE);
        }else{
            time--;
        }
        refreshRun(1000);
    }
}